package com.example.tp3;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.example.tp3.data.Team;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/searchteams.php?l=idLeague&s=yearsBYearsE
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerLeague {

    private static final String TAG = JSONResponseHandlerLeague.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerLeague(Team team) {

        this.team = team;
    }

    public void teamsInLeague(JsonReader jsonReader) throws IOException {

        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nameTeam = jsonReader.nextName();
            if (nameTeam.equals("table")) {
                readArrayTeams(jsonReader);
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
    }

    private void readArrayTeams(JsonReader jsonReader) throws IOException {

        jsonReader.beginArray();

        int nb = 0;

        while (jsonReader.hasNext()) {
            jsonReader.beginObject();
            while (jsonReader.hasNext()) {
                String teamName = jsonReader.nextName();
                if (teamName.equals("teamid")) {
                    long teamid = jsonReader.nextLong();
                    if (teamid == team.getIdTeam()) {
                        team.setRanking(nb+1);
                        while (jsonReader.hasNext()) {
                            String string = jsonReader.nextName();
                            if (string.equals("total")) {
                                team.setTotalPoints(jsonReader.nextInt());
                            } else {
                                jsonReader.skipValue();
                            }
                        }
                    }
                } else {
                    jsonReader.skipValue();
                }
            }
            jsonReader.endObject();
            nb++;
        }
        jsonReader.endArray();
    }

    public void readJsonStream(InputStream insputStream) throws IOException {
        JsonReader jsonReader = new JsonReader(new InputStreamReader(insputStream, "UTF-8"));
        try {
            teamsInLeague(jsonReader);
        }
        finally {
            jsonReader.close();
        }
    }

}
