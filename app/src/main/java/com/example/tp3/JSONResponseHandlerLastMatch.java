package com.example.tp3;

import android.util.JsonReader;

import com.example.tp3.data.Match;
import com.example.tp3.data.Team;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerLastMatch {

    private static final  String TAG = JSONResponseHandlerLeague.class.getSimpleName();

    private Match match;
    private Team team;

    public JSONResponseHandlerLastMatch(Match match, Team team) {
        this.match = match;
        this.team = team;
    }

    public void readArrayTeams(JsonReader jsonReader) throws IOException {

        jsonReader.beginArray();
        int nb = 0;
        while (jsonReader.hasNext()) {
            jsonReader.beginObject();
            while (jsonReader.hasNext()) {
                String nameEvent = jsonReader.nextName();
                if (nb == 0) {
                    if (nameEvent.equals("idEvent")) {
                        match.setId(jsonReader.nextLong());
                    } else if (nameEvent.equals("strEvent")) {
                        match.setLabel(jsonReader.nextString());
                    } else if (nameEvent.equals("strHomeTeam")) {
                        match.setHomeTeam(jsonReader.nextString());
                    } else if (nameEvent.equals("strAwayTeam")) {
                        match.setAwayTeam(jsonReader.nextString());
                    } else if (nameEvent.equals("intHomeScore")) {
                        match.setHomeScore(jsonReader.nextInt());
                    } else if (nameEvent.equals("intAwayScore")) {
                        match.setAwayScore(jsonReader.nextInt());
                    } else {
                        jsonReader.skipValue();
                    }
                } else {
                    jsonReader.skipValue();
                }
            }
            jsonReader.endObject();
            nb++;
        }
        team.setLastEvent(match);
        jsonReader.endArray();
    }

    public void readTeams(JsonReader jsonReader) throws IOException {

        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nameTeam = jsonReader.nextName();
            if (nameTeam.equals("result")) {
                readArrayTeams(jsonReader);
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
    }


    public void readJsonStream(InputStream inputStream) throws IOException {

        JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
        try {
            readTeams(jsonReader);
        }
        finally {
            jsonReader.close();
        }
    }

}
