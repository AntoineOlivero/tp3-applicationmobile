package com.example.tp3;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tp3.data.Match;
import com.example.tp3.data.Team;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
//import com.example.tp3.webservice.WebServiceGet;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;


    private int totalPoints;
    private int ranking;
    private Match lastMatch;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;
    private  Match match;

    private boolean ajout;
    private boolean update;
    private long id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra(Team.TAG);
        lastMatch = new Match();
        ajout = (boolean) getIntent().getExtras().getBoolean("add");
        update = (boolean) getIntent().getExtras().getBoolean("updateAll");

        id = team.getId();

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        updateView();

        if (update) {
            new UpdateFunction().execute();
            Intent intent = new Intent(TeamActivity.this, MainActivity.class);
            intent.putExtra(Team.TAG, team);
            intent.putExtra("add", false);
            setResult(1, intent);
            finish();
        } else {
            final Button but = (Button) findViewById(R.id.button);
            but.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    new UpdateFunction().execute();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity
        Intent intent = new Intent(TeamActivity.this, MainActivity.class);
        intent.putExtra(Team.TAG, team);
        setResult(1, intent);
        finish();
        super.onBackPressed();
    }

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

        //TODO : update imageBadge
        new TaskRecupImage().execute();
    }

    private class UpdateFunction extends AsyncTask<TextView, String, Team> {

        protected Team doInBackground(TextView... textViews) {
            try {
                URL url = WebServiceUrl.buildSearchTeam(team.getName());
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                try {
                    InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                    JSONResponseHandlerTeam jsonResponseHandlerTeam = new JSONResponseHandlerTeam(team);
                    jsonResponseHandlerTeam.readJsonStream(inputStream);
                    url = WebServiceUrl.buildGetRanking(team.getIdLeague());
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                }finally {
                    httpURLConnection.disconnect();
                }
                try {
                    InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                    JSONResponseHandlerLeague jsonResponseHandlerLeague = new JSONResponseHandlerLeague(team);
                    jsonResponseHandlerLeague.readJsonStream(inputStream);
                    url = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                }
                finally {
                    httpURLConnection.disconnect();
                }
                try {
                    InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                    JSONResponseHandlerLastMatch jsonResponseHandlerLastMatch = new JSONResponseHandlerLastMatch(lastMatch, team);
                    jsonResponseHandlerLastMatch.readJsonStream(inputStream);
                }
                finally {
                    httpURLConnection.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return team;
        }




        @Override
        protected void onPostExecute(Team team) {
            super.onPostExecute(team);
            updateView();

           /* Team teamChanged = new Team(team.getName(), team.getLeague());
            Intent intent = new Intent(TeamActivity.this, MainActivity.class);
            intent.putExtra(Team.TAG, (Parcelable) teamChanged);
            if (ajout) {
                intent.putExtra("add", true);
            } else {
                intent.putExtra("add", false);
            }
            setResult(1, intent);
            finish();*/
        }
    }

    public class TaskRecupImage extends AsyncTask<String,Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... string) {
            Bitmap bitmap = null;
            try{
                URL url = new URL(team.getTeamBadge());
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                    bitmap = BitmapFactory.decodeStream(is);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap bm){
            super.onPostExecute(bm);
            imageBadge.setImageBitmap(bm);
        }
    }
}
