package com.example.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.tp3.data.Match;
import com.example.tp3.data.SportDbHelper;
import com.example.tp3.data.Team;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Parcelable;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    final String[] columnTeam = new String[] {
            SportDbHelper.COLUMN_TEAM_NAME,
            SportDbHelper.COLUMN_LEAGUE_NAME
    };
    public SimpleCursorAdapter simpleCursorAdapter;
    public ListView listViewTeam;
    public SportDbHelper sportDbHelper;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Button flotant +
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Team team = new Team("");
                Intent intent = new Intent(MainActivity.this, NewTeamActivity.class);
                intent.putExtra(team.TAG, (Parcelable) team);
                intent.putExtra("ajoutTeam", true);
                startActivityForResult(intent, 1);
            }
        });

        //adaptateur
        sportDbHelper = new SportDbHelper(this);
        SQLiteDatabase sqLiteDatabase = sportDbHelper.getReadableDatabase();
        Cursor mCursor = sqLiteDatabase.rawQuery("SELECT * FROM " + SportDbHelper.TABLE_NAME, null);
        if(mCursor.getCount() == 0) {
            sportDbHelper.populate();
        }

        final Cursor cursor = sportDbHelper.fetchAllTeams();
        simpleCursorAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor, columnTeam, new int[]{android.R.id.text1, android.R.id.text2}, 0);


        listViewTeam = findViewById(R.id.listView_Equipe);
        listViewTeam.setAdapter(simpleCursorAdapter);


        //transition MainActivity -> TeamActivity
        listViewTeam.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursorDest = (Cursor) parent.getItemAtPosition(position);
                Team teamSelected = SportDbHelper.cursorToTeam(cursorDest);

                Intent teamActivity = new Intent(MainActivity.this, TeamActivity.class);
                teamActivity.putExtra(Team.TAG, (Parcelable) teamSelected);
                startActivityForResult(teamActivity, 1);
            }
        });

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new RefreshTask().execute();
            }
        });
    }

    @Override
    public void onActivityResult(int request, int result, Intent intent) {
        super.onActivityResult(request, result, intent);
        if (intent != null) {
            Team team = intent.getParcelableExtra(Team.TAG);
            boolean bool = (intent.getExtras()).getBoolean("add");
            if (bool) {
                boolean boolInsert = sportDbHelper.addTeam(team);
                if (boolInsert) {
                    simpleCursorAdapter.changeCursor(sportDbHelper.fetchAllTeams());
                    simpleCursorAdapter.notifyDataSetChanged();
                } else {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((MainActivity.this));
                    alertDialogBuilder.setTitle("Ajout impossible");
                    alertDialogBuilder.setMessage("Cette équipe éxiste déjà");
                    alertDialogBuilder.create();
                    alertDialogBuilder.show();
                }
            } else{
                sportDbHelper.updateTeam(team);
            }
        }
        simpleCursorAdapter.changeCursor(sportDbHelper.fetchAllTeams());
        simpleCursorAdapter.notifyDataSetChanged();
    }

    public void onResume() {
        super.onResume();
    }

    //menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class RefreshTask extends AsyncTask<TextView, String, List<Team>> {

        private List<Team>  teamList;
        private Match lastEvent = new Match();


        public List<Team> doInBackground(TextView... textViews) {
            teamList = sportDbHelper.getAllTeams();
            for (Team team : teamList) {
                boolean bool = true;
                while(bool){
                    URL url = null;
                    HttpURLConnection httpURLConnection = null;
                    try {
                        url = WebServiceUrl.buildSearchTeam(team.getName());
                        httpURLConnection = (HttpURLConnection) url.openConnection();
                        JSONResponseHandlerTeam jsonTeam = new JSONResponseHandlerTeam(team);
                        try {
                            InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                            jsonTeam.readJsonStream(inputStream);
                            url = WebServiceUrl.buildGetRanking(team.getIdLeague());
                            httpURLConnection = (HttpURLConnection) url.openConnection();
                        } finally {
                            if (!jsonTeam.exist){
                                break;
                            }
                            httpURLConnection.disconnect();
                        }
                        try {
                            InputStream is = new BufferedInputStream(httpURLConnection.getInputStream());
                            JSONResponseHandlerLeague jsonLeague = new JSONResponseHandlerLeague(team);
                            jsonLeague.readJsonStream(is);
                            url = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                            httpURLConnection = (HttpURLConnection) url.openConnection();
                        } finally {
                            httpURLConnection.disconnect();
                        }
                        try {
                            InputStream is = new BufferedInputStream(httpURLConnection.getInputStream());
                            JSONResponseHandlerLastMatch jsonResponseHandlerLastMatch = new JSONResponseHandlerLastMatch(lastEvent, team);
                            jsonResponseHandlerLastMatch.readJsonStream(is);
                        } finally {
                            httpURLConnection.disconnect();
                            bool = false;
                        }
                    } catch (MalformedURLException e) { //for buildSearchTeam
                        e.printStackTrace();
                    } catch (IOException e) {   //for openConnection
                        e.printStackTrace();
                    }
                }
            }
            return teamList;
        }

        public void onPostExecute(List<Team> teamList) {
            super.onPostExecute(teamList);
            for (Team team : teamList) {
                sportDbHelper.updateTeam(team);
            }
            simpleCursorAdapter.changeCursor(sportDbHelper.fetchAllTeams());
            simpleCursorAdapter.notifyDataSetChanged();
            swipeRefreshLayout.setRefreshing(false);
        }
    }


}
